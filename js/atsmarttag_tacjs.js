/**
 * AT Internet smarttag service
 */
(($, Drupal, drupalSettings, tarteaucitron) => {
  // Process only if atsmarttag settings and code have been defined.
  if (
    drupalSettings.atsmarttag === undefined ||
    Drupal.atsmarttag === undefined
  ) {
    return;
  }

  // Process only if the atinternet_smarttag tacjs service is enabled.
  const smarttagService = drupalSettings.tacjs.services.atinternet_smarttag;
  if (smarttagService && smarttagService.status) {
    // Disable default AT Internet SmartTag behavior.
    Drupal.behaviors.atsmarttag.attach = () => {};

    // AT Internet SmartTag service definition.
    tarteaucitron.services.atinternet_smarttag = {
      key: 'atinternet_smarttag',
      type: 'analytic',
      name: 'AT Internet (SmartTag)',
      uri: 'https://helpcentre.atinternet-solutions.com/hc/fr/categories/360002439300-Privacy-Centre',
      needConsent: true,
      cookies: [
        'atidvisitor',
        'atreman',
        'atredir',
        'atsession',
        'atuserid',
        'attvtreman',
        'attvtsession',
      ],
      js() {
        Drupal.atsmarttag.createTagAndDispatch(drupalSettings.atsmarttag);
      },
      fallback() {
        // TODO
      },
    };
  }
})(jQuery, Drupal, drupalSettings, window.tarteaucitron);

# AT Internet SmartTag for TacJS

Allows handling of the AT Internet SmartTag service by the TacJS module.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/atsmarttag_tacjs).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/atsmarttag_tacjs).


## Requirements

This module requires the following modules:

- [AT Internet SmartTag](https://www.drupal.org/project/atsmarttag)
- [TacJS](https://www.drupal.org/project/tacjs)

## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

The module has no menu or modifiable settings.

You only need to activate the **AT Internet SmartTag** service in the TacJS
module configuration.


## Maintainers

- Frank Mably - [mably](https://www.drupal.org/u/mably)
